﻿using System.Collections.Generic;

namespace Application.Models
{
    public class Usuario
    {
        public string id { get; set; }
        public string nome { get; set; }
        public string email { get; set; }
        public string senha { get; set; }

        public string data_criacao { get; set; }
        public string data_atualizacao { get; set; }
        public string ultimo_login { get; set; }

        public string token { get; set; }

        public IList<Telefone> telefones { get; set; } = new List<Telefone>();
    }
}
