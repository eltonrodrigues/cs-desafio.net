﻿namespace Application.Models
{
    public class Telefone
    {
        public string ddd { get; set; }
        public string numero { get; set; }
    }
}
