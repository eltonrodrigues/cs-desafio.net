﻿using Application.Models;

namespace Application.Services.Especifications
{
    public interface IUsuarioService
    {
        string Token { get; set; }
        Usuario Login(string email, string senha);
        Usuario SignUp(Usuario user);
        Usuario Profile(string id);
    }
}
