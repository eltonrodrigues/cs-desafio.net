﻿using Application.Models;
using Application.Services.Especifications;
using Domain.Services.Especifications;

namespace Application.Services.Implementations
{
    public class UsuarioService : Mapper.Converter<Domain.Entities.User, Application.Models.Usuario>, IUsuarioService
    {
        public string Token { get; set; }
        private IUserService service;

        public UsuarioService(IUserService service)
        {
            this.service = service;
        }
        

        Usuario IUsuarioService.Login(string email, string senha)
        {
            return DomainToApplication(service.SignIn(email, senha));
        }

        Usuario IUsuarioService.Profile(string id)
        {
            service.Security.Token = Token;

            return DomainToApplication(service.GetBy(id));
        }

        Usuario IUsuarioService.SignUp(Usuario usuario)
        {
            return DomainToApplication(service.SignUp(ApplicationToDomain(usuario)));
        }
    }
}
