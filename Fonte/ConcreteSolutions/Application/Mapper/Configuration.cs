﻿using AutoMapper;

namespace Application.Mapper
{


    public class Configuration
    {
        public static IMapper Mapper;

        public static void Setup()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Entities.User, Models.Usuario>()
                    .ForMember(to => to.id, from=> from.MapFrom(src=> src.Id))
                    .ForMember(to => to.nome, from=> from.MapFrom(src=> src.Name))
                    .ForMember(to => to.email, from=> from.MapFrom(src=> src.Email))
                    .ForMember(to => to.senha, from=> from.MapFrom(src=> src.Password))
                    .ForMember(to => to.token, from=> from.MapFrom(src=> src.Token))
                    .ForMember(to => to.data_criacao, from=> from.MapFrom(src=> src.Create.ToString("dd/MM/yyyy HH:mm:sss")))
                    .ForMember(to => to.data_atualizacao, from=> from.MapFrom(src=> src.Update.ToString("dd/MM/yyyy HH:mm:sss")))
                    .ForMember(to => to.ultimo_login, from=> from.MapFrom(src=> src.LastSignin.ToString("dd/MM/yyyy HH:mm:sss")))
                    .ForMember(to => to.telefones, from=> from.MapFrom(src=> src.Telephones));

                cfg.CreateMap<Models.Usuario, Domain.Entities.User>()
                    .ForMember(to => to.Id, from=> from.MapFrom(src=> src.id))
                    .ForMember(to => to.Name, from=> from.MapFrom(src=> src.nome))
                    .ForMember(to => to.Email, from=> from.MapFrom(src=> src.email))
                    .ForMember(to => to.Password, from=> from.MapFrom(src=> src.senha))
                    .ForMember(to => to.Telephones, from=> from.MapFrom(src=> src.telefones));

                cfg.CreateMap<Domain.Entities.Telephone, Models.Telefone>()
                    .ForMember(to => to.ddd, from=> from.MapFrom(src=> src.DDD))
                    .ForMember(to => to.numero, from=> from.MapFrom(src=> src.Number));

                cfg.CreateMap<Models.Telefone, Domain.Entities.Telephone>()
                    .ForMember(to => to.DDD, from=> from.MapFrom(src=> src.ddd))
                    .ForMember(to => to.Number, from=> from.MapFrom(src=> src.numero));
            });

            Mapper = config.CreateMapper();
        }
    }
}
