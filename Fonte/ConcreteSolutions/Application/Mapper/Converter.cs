﻿using Application.Mapper.Especifications;

namespace Application.Mapper
{
    public abstract class Converter<TDomainEntity, TApplicationEntity> : IConverter<TDomainEntity, TApplicationEntity>
        where TDomainEntity : class
        where TApplicationEntity : class
    {
        public TDomainEntity ApplicationToDomain(TApplicationEntity entity)
        {
            return Mapper.Configuration.Mapper.Map<TDomainEntity>(entity);
        }

        public TApplicationEntity DomainToApplication(TDomainEntity entity)
        {
            return Mapper.Configuration.Mapper.Map<TApplicationEntity>(entity);
        }
    }
}
