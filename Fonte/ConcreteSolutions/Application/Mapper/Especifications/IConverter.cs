﻿namespace Application.Mapper.Especifications
{
    public interface IConverter<TDomainEntity, TApplicationEntity> 
        where TDomainEntity : class
        where TApplicationEntity : class
    {
        TDomainEntity ApplicationToDomain(TApplicationEntity entity);
        TApplicationEntity DomainToApplication(TDomainEntity entity);
    }
}
