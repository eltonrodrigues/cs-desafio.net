﻿using API.Interceptors;
using System.Web.Http;
using System.Web.Http.Cors;

namespace API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            Application.Mapper.Configuration.Setup();
            DependencyInjection.Configuration.Setup();
            
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Enable CORS            
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Force only JSON            
            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            config.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);


            config.Filters.Add(new ExceptionInterceptorAttribute());
        }
    }
}
