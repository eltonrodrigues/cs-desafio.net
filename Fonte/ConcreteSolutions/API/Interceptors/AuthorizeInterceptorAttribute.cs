﻿using Common.Exceptions;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace API.Interceptors
{
    public class AuthorizeInterceptorAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!actionContext.Request.Headers.Contains("Authorization"))
                throw new NotAuthorizeException();
        }
    }
}