﻿using Common.Exceptions;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace API.Interceptors
{
    public class ExceptionInterceptorAttribute  : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is ICustomException)
            {
                var ex = actionExecutedContext.Exception as ICustomException;

                CreateResponseError(actionExecutedContext, (HttpStatusCode)ex.Code, ex.Message);
            }
            else if (actionExecutedContext.Exception != null)
                CreateResponseError(actionExecutedContext, HttpStatusCode.InternalServerError, actionExecutedContext.Exception.Message);
        }

        private void CreateResponseError(HttpActionExecutedContext context, HttpStatusCode statusCode, string message)
        {
            var retorno = new
            {
                statusCode = (int)statusCode,
                mensagem   = message
            };

            context.Response = context.Request.CreateResponse(statusCode, retorno);
        }
    }
}