﻿using Application.Models;
using Application.Services.Especifications;
using System.Threading.Tasks;

namespace API.Controllers
{
    public class ProfileController : BaseAuthorizedController
    {
        private IUsuarioService service;

        public ProfileController(IUsuarioService service)
        {
            this.service = service;
        }

        public async Task<Usuario> Get(string id)
        {
            return await Task<Usuario>.Run(() =>
            {
                service.Token = Token;

                return service.Profile(id);

            });
        }
    }
}
