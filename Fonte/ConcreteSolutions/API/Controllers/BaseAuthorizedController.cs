﻿using API.Interceptors;
using System.Linq;
using System.Web.Http;

namespace API.Controllers
{
    [AuthorizeInterceptor]
    public class BaseAuthorizedController : ApiController
    {
        public string Token
        {
            get
            {
                if (Request != null && Request.Headers.Contains("Authorization"))
                {
                    var value = Request.Headers.GetValues("Authorization").FirstOrDefault();
                    return value.Replace(" ", "").Replace("Bearer", "");
                }
                
                return null;
            }
        }
    }
}
