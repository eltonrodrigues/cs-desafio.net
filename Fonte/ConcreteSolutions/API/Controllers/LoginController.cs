﻿using Application.Models;
using Application.Services.Especifications;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    public class LoginController : ApiController
    {
        private IUsuarioService service;

        public LoginController(IUsuarioService service)
        {
            this.service = service;
        }

        public async Task<Usuario> Post(string email, string senha)
        {
            return await Task<Usuario>.Run(() =>
            {
                return service.Login(email, senha);
            });
        }
    }
}
