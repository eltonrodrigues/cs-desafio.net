﻿using Application.Models;
using Application.Services.Especifications;
using System.Threading.Tasks;
using System.Web.Http;

namespace API.Controllers
{
    public class SignUpController : ApiController
    {
        private IUsuarioService service;

        public SignUpController(IUsuarioService service)
        {
            this.service = service;
        }

        public async Task<Usuario> Post(Usuario usuario)
        {
            return await Task<Usuario>.Run(() =>
            {
                return service.SignUp(usuario);
            });
        }
    }
}
