﻿namespace Common.Exceptions
{
    public class NotUniqueException : System.Exception, ICustomException
    {
        public int Code
        {
            get
            {
                return 400;
            }
        }

        public NotUniqueException(string message) : base(message)
        {

        }
    }
}
