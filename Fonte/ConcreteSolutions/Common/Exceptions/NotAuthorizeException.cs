﻿namespace Common.Exceptions
{
    public class NotAuthorizeException : System.Exception, ICustomException
    {
        public int Code
        {
            get
            {
                return 401;
            }
        }

        public NotAuthorizeException() : base("Não autorizado")
        {
            
        }

        public NotAuthorizeException(string message) :base(message)
        {

        }
    }
}
