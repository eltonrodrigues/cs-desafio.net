﻿namespace Common.Exceptions
{
    public class MandatoryFieldsException : System.Exception, ICustomException
    {
        public int Code { get { return 400; } }

        public MandatoryFieldsException(string message) : base(message)
        {

        }
    }
}
