﻿namespace Common.Exceptions
{
    public interface ICustomException
    {
        int Code { get; }
        string Message { get; }
    }
}
