﻿using Application.Services.Especifications;
using Application.Services.Implementations;
using Domain.Repositories.Especifications;
using Domain.Services.Especifications;
using Domain.Services.Implementations;
using Infra.DatabaseContext;
using Infra.Repositories;
using SimpleInjector;
using SimpleInjector.Integration.WebApi;
using System.Data.Entity;
using System.Web.Http;

namespace DependencyInjection
{
    public class Configuration
    {
        public static void Setup()
        {
            var container = new Container();

            container.Options.DefaultScopedLifestyle = new WebApiRequestLifestyle();

            //Infra
            container.Register<DbContext, ConcreteSolutionsContext>(Lifestyle.Scoped);
            //Repository
            container.Register<IUserRepository, UserRepository>();
            //Domain
            container.Register<IUserService, UserService>();
            container.Register<ISecurity, SecurityService>();
            //Application
            container.Register<IUsuarioService, UsuarioService>();


            container.RegisterWebApiControllers(GlobalConfiguration.Configuration);

            container.Verify();

            GlobalConfiguration.Configuration.DependencyResolver =
                new SimpleInjectorWebApiDependencyResolver(container);
        }
    }
}
