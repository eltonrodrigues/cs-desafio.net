﻿using Domain.Entities;
using Infra.EntityConfigurations;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.SqlServer;
using System.Diagnostics;
using System.Linq;

namespace Infra.DatabaseContext
{
    public class ConcreteSolutionsContext : DbContext
    {
        private SqlProviderServices provider;
        public DbSet<User> Users { get; set; }
        public DbSet<Telephone> Telephones { get; set; }

        public ConcreteSolutionsContext() : base("ConcreteSolutionsContext")
        {
            provider = SqlProviderServices.Instance;

            Database.Log = (value) =>
            {
                Trace.TraceInformation(value);
            };
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new TelephoneConfiguration());

            base.OnModelCreating(modelBuilder);

        }

        public override int SaveChanges()
        {
            foreach (var newItem in ChangeTracker.Entries().Where(x => x.State == EntityState.Added))
            {
                newItem.Entity
                    .GetType()
                    .GetProperty("Id")
                    .SetValue(newItem.Entity, Guid.NewGuid().ToString());
            } 
            
            return base.SaveChanges();
        }
    }
}
