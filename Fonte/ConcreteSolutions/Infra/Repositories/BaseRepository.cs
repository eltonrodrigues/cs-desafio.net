﻿using Domain.Repositories.Especifications;
using System.Data.Entity;
using System.Linq;

namespace Infra.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected DbContext context;

        public BaseRepository(DbContext context)
        {
            this.context = context;
        }

        public virtual IQueryable<TEntity> Query()
        {
            return context.Set<TEntity>().AsQueryable();
        }

        public virtual TEntity Insert(TEntity entity, bool saveChanges = true)
        {
            context.Set<TEntity>().Add(entity);

            if (saveChanges)
                Save();

            return entity;
        }

        public virtual TEntity Update(TEntity entity, bool saveChanges = true)
        {
            context.Entry(entity).State = EntityState.Modified;

            if (saveChanges)
                Save();

            return entity;
        }

        public virtual void Save()
        {
            context.SaveChanges();
        }
    }
}
