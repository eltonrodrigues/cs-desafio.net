﻿using Common.Exceptions;
using Common.Util;
using Domain.Entities;
using Domain.Repositories.Especifications;
using System;
using System.Data.Entity;
using System.Linq;

namespace Infra.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {
        
        }

        public virtual User Create(User user)
        {
            if (!user.IsValid())
                throw new MandatoryFieldsException("Campos obrigatórios inválidos");

            if (Exists(user))
                throw new NotUniqueException("E-mail já existente");

            user.Token      = Guid.NewGuid().ToString();
            user.Create     = DateTime.Now;
            user.Update     = DateTime.Now;
            user.LastSignin = DateTime.Now;
            user.Password   = user.Password.Encrypt();

            base.Insert(user);

            return user;
        }

        public virtual User Update(User user)
        {
            if (!user.IsValid())
                throw new MandatoryFieldsException("Campos obrigatórios inválidos");

            if (Exists(user, true))
                throw new NotUniqueException("E-mail já existente");

            user.Update = DateTime.Now;

            return base.Update(user);
        }

        public virtual User SignIn(string email, string password)
        {
            var passwordCrypto = password.Encrypt();

            var user = base.Query().SingleOrDefault(u => 
                                        u.Email.Equals(email) && 
                                        u.Password.Equals(passwordCrypto));
            if (user == null)
                throw new NotAuthorizeException("Usuário e/ou senha inválidos.");
            else
            {
                user.Token      = Guid.NewGuid().ToString();
                user.LastSignin = DateTime.Now;

                Update(user);
            }

            return user;
        }

        public virtual User GetById(string id)
        {
            return base.Query().SingleOrDefault(u => u.Id == id);
        }

        public virtual User GetByToken(string token)
        {
            return base.Query().SingleOrDefault(u => u.Token.Equals(token));
        }

        private bool Exists(User user, bool isUpdate = false)
        {
            var query =  Query().Where(x=> x.Email == user.Email);

            if (isUpdate)
                query = query.Where(x => x.Id != user.Id);

            return query.Any();
        }
    }
}
