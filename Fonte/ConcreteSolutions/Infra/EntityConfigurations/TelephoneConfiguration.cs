﻿using Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Infra.EntityConfigurations
{
    public class TelephoneConfiguration : EntityTypeConfiguration<Telephone>
    {
        public TelephoneConfiguration()
        {
            HasKey(t => t.Id);

            Property(t => t.Id)
                .HasColumnType("CHAR")
                .HasMaxLength(36);

            Property(t => t.DDD)
                .HasMaxLength(3)
                .IsRequired();

            Property(t => t.Number)
                .HasMaxLength(10)
                .IsRequired();

            HasRequired(t => t.User)
                .WithMany(u => u.Telephones)
                .HasForeignKey(t => t.User_Id);
        }
    }
}
