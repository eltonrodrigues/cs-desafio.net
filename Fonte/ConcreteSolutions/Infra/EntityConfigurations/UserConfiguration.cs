﻿using Domain.Entities;
using System.Data.Entity.ModelConfiguration;

namespace Infra.EntityConfigurations
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(u => u.Id);

            Property(u => u.Id)
                .HasColumnType("CHAR")
                .HasMaxLength(36);

            Property(u => u.Name)
                .HasMaxLength(100)
                .IsRequired();

            Property(u => u.Email)
                .HasMaxLength(100)
                .IsRequired();

            Property(u => u.Password)
                .HasMaxLength(100)
                .IsRequired();

            Property(u => u.Token)
                .HasColumnType("CHAR")
                .HasMaxLength(36)
                .IsRequired();

            Property(u => u.Create).IsRequired();
            Property(u => u.Update);
            Property(u => u.LastSignin);
        }
    }
}
