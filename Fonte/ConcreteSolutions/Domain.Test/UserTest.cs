﻿using Common.Exceptions;
using Domain.Entities;
using Domain.Services.Implementations;
using Infra.DatabaseContext;
using Infra.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Domain.Test
{
    [TestClass]
    public class UserTest
    {
        private Mock<ConcreteSolutionsContext> mockContext;
        private SecurityService securityService;
        private UserRepository userRepository;
        private UserService userService;
        private List<User> users;
        private Mock<DbSet<User>> mockSet;

        [TestInitialize]
        public void Initialize()
        {
            CreateInstances();
            CreateData();
            ConfigureMock();
        }
        
        private void CreateInstances()
        {
            this.mockSet         = new Mock<DbSet<User>>();
            this.mockContext     = new Mock<ConcreteSolutionsContext>();
            this.userRepository  = new UserRepository(mockContext.Object);
            this.securityService = new SecurityService(userRepository);
            this.userService     = new UserService(userRepository, securityService);
        }
        private void CreateData()
        {
            this.users = new List<User>();

            this.users.Add(new User()
            {
                Id         = "6103073F-8C51-4527-A528-D22DACFB6C92",
                Name       = "John Doe",
                Email      = "johndoe@gmail.com",
                Password   = "827ccb0eea8a706c4c34a16891f84e7b",//12345
                Create     = DateTime.Now,
                Update     = DateTime.Now,
                LastSignin = DateTime.Now,
                Token      = "64A12FCA-045D-4428-974B-76F6AC1AE2DD",
            });

        }

        private void ConfigureMock()
        {
            mockSet.As<IQueryable<User>>().Setup(m => m.Provider).Returns(users.AsQueryable().Provider);
            mockSet.As<IQueryable<User>>().Setup(m => m.Expression).Returns(users.AsQueryable().Expression);
            mockSet.As<IQueryable<User>>().Setup(m => m.ElementType).Returns(users.AsQueryable().ElementType);
            mockSet.As<IQueryable<User>>().Setup(m => m.GetEnumerator()).Returns(users.AsQueryable().GetEnumerator());

            mockSet.Setup(m => m.Find(It.IsAny<object[]>()))
               .Returns<object[]>(ids => users.FirstOrDefault(d => d.Id == ids[0].ToString()));

            mockSet.Setup(m => m.Add(It.IsAny<User>()))
               .Callback<User>((obj) => { users.Add(obj); });

            mockContext.Setup(c => c.Set<User>()).Returns(mockSet.Object);
        }


        [TestMethod]
        public void MustBeSignUp()
        {
            var user = new User()
            {
                Name     = "New User",
                Email    = "new@gmail.com",
                Password = "12345",
            };

            userService.SignUp(user);

            mockSet.Verify(x => x.Add(It.Is<User>(u => u.Name == user.Name)));
            mockContext.Verify(x => x.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void MustBeLogin()
        {
            var user = userService.SignIn("johndoe@gmail.com", "12345");

            Assert.IsNotNull(user);
        }


        [TestMethod]
        public void MustBeGetProfile()
        {
            userService.Security.Token = "64A12FCA-045D-4428-974B-76F6AC1AE2DD";
            var user = userService.GetBy("6103073F-8C51-4527-A528-D22DACFB6C92");

            Assert.IsNotNull(user);
        }


        [TestMethod]
        [ExpectedException(typeof(NotAuthorizeException))]
        public void DontMustBeLoginWithCredentialsInvalid()
        {
            userService.SignIn("teste@gmail.com", "12345");
        }

        [TestMethod]
        [ExpectedException(typeof(NotUniqueException))]
        public void DontMustSignupWithExistentEmail()
        {
            var user = new User()
            {
                Name = "New User",
                Email = "johndoe@gmail.com",
                Password = "12345",
            };

            userService.SignUp(user);
        }

        [TestMethod]
        [ExpectedException(typeof(MandatoryFieldsException))]
        public void DontMustSignupWithoutName()
        {
            var user = new User()
            {
                Email = "johndoe@gmail.com",
                Password = "12345",
            };

            userService.SignUp(user);
        }

        [TestMethod]
        [ExpectedException(typeof(MandatoryFieldsException))]
        public void DontMustSignupWithoutEmail()
        {
            var user = new User()
            {
                Name = "New User",
                Password = "12345",
            };

            userService.SignUp(user);
        }

        [TestMethod]
        [ExpectedException(typeof(MandatoryFieldsException))]
        public void DontMustSignupWithoutPassword()
        {
            var user = new User()
            {
                Name = "New User",
                Email = "johndoe@gmail.com",
            };

            userService.SignUp(user);
        }

        [TestMethod]
        [ExpectedException(typeof(NotAuthorizeException))]
        public void DontMustBeGetProfileWithoutToken()
        {
            userService.GetBy("6103073F-8C51-4527-A528-D22DACFB6C92");
        }

        [TestMethod]
        [ExpectedException(typeof(NotAuthorizeException))]
        public void DontMustBeGetProfileWithTokenExpired()
        {
            users[0].LastSignin = DateTime.Now.AddMinutes(-40);

            userService.Security.Token = "64A12FCA-045D-4428-974B-76F6AC1AE2DD";
            userService.GetBy("6103073F-8C51-4527-A528-D22DACFB6C92");
        }
    }
}
