﻿using Domain.Entities;
using Domain.Repositories.Especifications;
using Domain.Services.Especifications;

namespace Domain.Services.Implementations
{
    public class UserService : IUserService
    {
        private IUserRepository repository;
        private ISecurity security;

        public ISecurity Security
        {
            get
            {
                return security;
            }
        }

        public UserService(IUserRepository repository, ISecurity security)
        {
            this.repository = repository;
            this.security   = security;
        }

        public User GetBy(string id)
        {
            security.Verify();
            
            return repository.GetById(id);
        }

        public User SignIn(string email, string password)
        {
            return repository.SignIn(email, password);
        }

        public User SignUp(User user)
        {
            return repository.Create(user);
        }
    }
}
