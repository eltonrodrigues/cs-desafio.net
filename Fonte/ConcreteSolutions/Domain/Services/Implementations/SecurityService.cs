﻿using Common.Exceptions;
using Domain.Repositories.Especifications;
using Domain.Services.Especifications;
using System;

namespace Domain.Services.Implementations
{
    public class SecurityService : ISecurity
    {
        protected string token;
        private IUserRepository repository;

        public SecurityService(IUserRepository repository)
        {
            this.repository = repository;
        }

        public string Token
        {
            get
            {
                return token;
            }
            set
            {

                token = value;
            }
        }

        public void Verify()
        {
            if (string.IsNullOrEmpty(token))
                throw new NotAuthorizeException();

            var user = repository.GetByToken(token);

            if(user == null)
                throw new NotAuthorizeException();

            if(user.LastSignin.AddMinutes(30) < DateTime.Now)
                throw new NotAuthorizeException("Sessão inválida");
        }
    }
}
