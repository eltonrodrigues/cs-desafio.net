﻿namespace Domain.Services.Especifications
{
    public interface ISecurity
    {
        string Token { get; set; }
        void Verify();
    }
}
