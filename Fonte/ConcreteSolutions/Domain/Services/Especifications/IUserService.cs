﻿using Domain.Entities;

namespace Domain.Services.Especifications
{
    public interface IUserService
    {
        ISecurity Security { get; }
        User GetBy(string id);
        User SignUp(User user);
        User SignIn(string email, string password);
    }
}
