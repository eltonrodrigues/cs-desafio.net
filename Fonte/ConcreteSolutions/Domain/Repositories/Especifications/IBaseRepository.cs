﻿using System.Linq;

namespace Domain.Repositories.Especifications
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> Query();
        TEntity Insert(TEntity entity, bool saveChanges = true);
        TEntity Update(TEntity entity, bool saveChanges = true);
        void Save();
    }
}
