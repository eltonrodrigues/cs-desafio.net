﻿using Domain.Entities;

namespace Domain.Repositories.Especifications
{
    public interface IUserRepository
    {
        User Create(User user);
        User Update(User user);

        User GetById(string id);

        User GetByToken(string token);

        User SignIn(string email, string password);
    }
}
