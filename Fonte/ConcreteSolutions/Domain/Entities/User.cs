﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime Create { get; set; }
        public DateTime Update { get; set; }
        public DateTime LastSignin { get; set; }

        public string Token { get; set; }

        public virtual IList<Telephone> Telephones { get; set; } = new List<Telephone>();

        public bool IsValid()
        {
            return
                !string.IsNullOrEmpty(Name) &&
                !string.IsNullOrEmpty(Email) &&
                !string.IsNullOrEmpty(Password);
        }
    }
}
