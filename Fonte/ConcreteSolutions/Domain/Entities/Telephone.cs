﻿namespace Domain.Entities
{
    public class Telephone
    {
        public string Id { get; set; }
        
        public string DDD { get; set; }
        public string Number { get; set; }

        public string User_Id { get; set; }
        public virtual User User { get; set; }
    }
}
